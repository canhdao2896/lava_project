#!/bin/bash

echo "[test_start:K-BSP_016_01_001]"

result=`./K_BSP_TP`

if [ $? = "0" ]
then
if [ $result = "OK" ]
then
	echo "[test_result:OK]"
	echo "[test_exit]"
	exit 0
else
	echo "[test_log:TPREL_K-BSP_016_01_001_$result:Expected value mismatch with Real value. OK vs $result]"
	echo "[test_result:NG]" 
	echo "[test_exit]"
	exit 0
fi
else
	exit 1
fi
