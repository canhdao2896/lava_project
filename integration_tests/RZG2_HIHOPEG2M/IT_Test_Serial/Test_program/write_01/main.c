/*
* Project: CIP LAVA IT test
* Test ID: write_01
* Feature: Checking API write of Serial Debug driver
* Sequence: open() -> configure -> write()
* Testing level: API user lib
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call write with valid open mode O_WRONLY; baudrate settings B9600; file descriptor and write buffer. Expected result = OK
*/

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int result = -1;

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct termios SerialPortSettings, SerialPortSettings_restore;	/* Create the structure */
	char write_buffer[] = "R";	/* Buffer containing characters to write into port */
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/ttySC0", O_WRONLY | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
								/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
								/* O_NOCTTY - No terminal will control the process */
								/* Open in blocking mode,read will wait */

	/*------------------ Setting the Attributes of the serial port using termios structure ---------------- */
	tcgetattr(fd, &SerialPortSettings);	/* Get the current attributes of the Serial port */
	tcgetattr(fd, &SerialPortSettings_restore);	/* Backup the current attributes of the Serial port */
	
	/* Setting the Baud rate */
	cfsetispeed(&SerialPortSettings, B9600);	/* Set Read Speed as B9600 */
	cfsetospeed(&SerialPortSettings, B9600);	/* Set Write Speed as B9600 */
	
	/* Setting 8N1 Control Mode */
	SerialPortSettings.c_cflag &= ~PARENB;	/* Disables the Parity Enable bit(PARENB), so No Parity */
	SerialPortSettings.c_cflag &= ~CSTOPB;	/* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit*/
	SerialPortSettings.c_cflag &= ~CSIZE;	/* Clears the mask for setting the data size		*/
	SerialPortSettings.c_cflag |= CS8;	/* Set the data bits = 8				*/
	SerialPortSettings.c_cflag &= ~CRTSCTS;	/* No Hardware flow Control				*/
	SerialPortSettings.c_cflag |= CREAD | CLOCAL;	/* Enable receiver,Ignore Modem Control lines	*/
	
	/* Setting Input Mode */
	SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);	/* Disable XON/XOFF flow control both i/p and o/p */
	
	/* Setting Local Mode */
	SerialPortSettings.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);	/* Non Cannonical mode, No echo, No signals */
	
	/* Setting Output Mode */
	SerialPortSettings.c_oflag &= ~OPOST;	/* No Output Post-processing */
	
	/* Discards old write/read data on the modem line and activate the settings for the port */
	tcflush(fd, TCIOFLUSH);
	tcsetattr(fd, TCSANOW, &SerialPortSettings);
	
	/*------------ Write data to serial port ----------------------*/
	result = write(fd, write_buffer, 1);	/* use write() to send data to port */
						/* "fd" - file descriptor pointing to the opened serial port */
						/* "write_buffer" - address of the buffer containing data */
						/* "sizeof(write_buffer)" - No of bytes to write */
	
	/* Check return value of sequence */
	if (result >= 0)
		printf ("OK\n");
	else if (result == -1)
		printf ("NG\n");
	else
		printf ("Unknown_result\n");
	
	/* Restore old port settings */
	tcsetattr(fd, TCSANOW, &SerialPortSettings_restore);

	/* Close tty device*/
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

