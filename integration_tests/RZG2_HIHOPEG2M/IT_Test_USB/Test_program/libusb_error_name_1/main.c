/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking libusb_error_name system call
* Sequence: libusb_error_name();
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_error_name of usblib library with error_code = LIBUSB_SUCCESS. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int result = 1;
	char* error_name;
	error_name = libusb_error_name(LIBUSB_SUCCESS);
	
	result = strcmp(error_name, "LIBUSB_SUCCESS / LIBUSB_TRANSFER_COMPLETED");
	
	if(result == 0)
		printf ("OK\n");
	else
		printf ("NG\n");
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

