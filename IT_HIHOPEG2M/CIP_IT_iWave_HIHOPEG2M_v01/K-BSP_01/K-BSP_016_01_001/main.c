/*
* Project: RZG_IT_CIP_BSP
* Test ID: K-BSP_016_01_001
* Feature: Checking open system call
* Sequence: open()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: K_BSP_TP.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874
* Details_description: Condition: Call open with device: /dev/i2c-0; mode: O_RDONLY. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	/* Call API or system call follow describe in PCL */
	result = open("/dev/i2c-0", O_RDONLY);	/*** I2C CHANNEL ***/

	/* Check return value of sequence */
	if (result >= 0) {
		printf("OK\n");
		close(result);					/*** I2C CLOSE ***/
	} else {
		printf("NG\n");
	}
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

