#!/bin/sh
#Function: Run all Test Program
#Author: AnhT
#Version:
#=========================================================
CURPATH=`pwd`

#LocNg: create new log file, backup the old one-----------------
if [ -f ./log_file.txt ]
then
	mv log_file.txt log_file_backup.txt
else
	touch log_file.txt
fi
#end modifying LocNg -------------------------------------------

for tc_dir in `ls -l | egrep "^d" | egrep BSP | awk '{print $NF}'`
 #LocNguyen: added |\.sh|\.csh|\.yaml
do
	echo $tc_dir
	cd $CURPATH/$tc_dir
	if [ -f "./test_config.sh" ]
	then
		./test_config.sh
	fi

	./runtest.sh $tc_dir #LocNguyen change name of .sh file, added $tc_dir

	if [ -f "./test_config_restore.sh" ]
	then
		./test_config_restore.sh
	fi

done
