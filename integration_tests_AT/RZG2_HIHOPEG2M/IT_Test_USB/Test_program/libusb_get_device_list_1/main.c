/*
* Project: CIP LAVA IT test
* Test ID: libusb_get_device_list_1
* Feature: Checking libusb_get_device_list system call
* Sequence: libusb_init();libusb_get_device_list()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_get_device_list of usblib library. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int result = -1;
	struct libusb_context *ctx;
	struct libusb_device **dev_list;

	libusb_init(&ctx);
	result = libusb_get_device_list(ctx, &dev_list);	//Expected: Number device in system

	if( result > 0 ) {
		printf ("OK\n");
		libusb_free_device_list(dev_list, 1);
		libusb_exit(ctx);
	}
	else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


