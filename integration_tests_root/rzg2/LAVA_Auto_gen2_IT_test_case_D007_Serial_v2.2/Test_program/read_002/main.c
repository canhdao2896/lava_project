/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking API read of Serial Debug driver
* Sequence: open() -> configure -> read()
* Testing level: API user lib
* Test-case type: Normal
* Expected result: OK
* Name: K_BSP_TP.c
* Author: RVC/HungDong (hung.dong.xd@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call read with valid open mode O_RDWR; baudrate settings B19200; Non parity; 1 stopbit; 8 bits datasize. Expected result = OK
*/

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>
#include <pthread.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int read_done = 0;
int result = -1;

/* Declare thread */
void *threadWrite(void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	int pins;
	struct termios SerialPortSettings, SerialPortSettings_restore;	/* Create the structure */
	char read_buffer[32];	/* Buffer to store the data received */
	pthread_t ptWrite;
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/ttySC0", O_RDWR | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
								/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
								/* O_NOCTTY - No terminal will control the process */
								/* Open in blocking mode,read will wait */

	/*------------------ Setting the Attributes of the serial port using termios structure ---------------- */
	tcgetattr(fd, &SerialPortSettings);	/* Get the current attributes of the Serial port */
	tcgetattr(fd, &SerialPortSettings_restore);	/* Backup the current attributes of the Serial port */
	
	/* Setting the Baud rate */
	cfsetispeed(&SerialPortSettings, B19200);	/* Set Read Speed as B19200 */
	cfsetospeed(&SerialPortSettings, B19200);	/* Set Write Speed as B19200 */
	
	/* Setting 8N1 Control Mode */
	SerialPortSettings.c_cflag &= ~PARENB;	/* Disables the Parity Enable bit(PARENB), so No Parity */
	SerialPortSettings.c_cflag &= ~CSTOPB;	/* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit*/
	SerialPortSettings.c_cflag &= ~CSIZE;	/* Clears the mask for setting the data size		*/
	SerialPortSettings.c_cflag |= CS8;	/* Set the data bits = 8				*/
	SerialPortSettings.c_cflag &= ~CRTSCTS;	/* No Hardware flow Control				*/
	SerialPortSettings.c_cflag |= CREAD | CLOCAL;	/* Enable receiver,Ignore Modem Control lines	*/
	
	/* Setting Input Mode */
	SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);	/* Disable XON/XOFF flow control both i/p and o/p */
	
	/* Setting Local Mode */
	SerialPortSettings.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);	/* Non Cannonical mode, No echo, No signals */
	
	/* Setting Output Mode */
	SerialPortSettings.c_oflag &= ~OPOST;	/* No Output Post-processing */
	
	/* Setting Time-out */
	SerialPortSettings.c_cc[VMIN] = 1;	/* Blocking read until 10 character arrives */
	SerialPortSettings.c_cc[VTIME] = 0;	/* Wait indefinetly (timer unused) */
	
	/* Discards old read/write data on the modem line and activate the settings for the port */
	tcflush(fd, TCIOFLUSH);
	tcsetattr(fd, TCSANOW, &SerialPortSettings);
	
	/* Setting Loopback function */
	ioctl( fd, TIOCMGET, &pins);	/* Obtain the modem status bits from the tty driver */
	pins = pins | 0x8000;	/* Set TIOCM_LOOP pin from register. This macro is defined at */
				/* <KERNEL_SRC>/include/uapi/asm-generic/termios.h */
	ioctl( fd, TIOCMSET, &pins);	/* Set the modem status bits from the tty driver */
	
	/*------------- Read data from serial port --------------*/
	pthread_create(&ptWrite, NULL, threadWrite, &fd);	/* Create and start write thread */
	
	result = read(fd, read_buffer, 1);	/* Read the data */
	read_done = 1;
	
	pthread_join(ptWrite, NULL);
	
	/* Check return value of sequence */
	if (result >= 0 && read_buffer[0] == 'R')
		printf ("OK\n");
	else if (result == -1 || read_buffer[0] != 'R')
		printf ("NG\n");
	else
		printf ("Unknown_result\n");
	
	/* Reset Loopback function */
	pins = 0x8000;
	ioctl(fd, TIOCMBIC, &pins);	/* Reset the modem status bits from the tty driver */
	
	/* Restore old port settings */
	tcsetattr(fd, TCSANOW, &SerialPortSettings_restore);
	
	/* Close tty device*/
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

void *threadWrite(void *arg){
	char write_buffer[] = "R";	/* Buffer containing characters to write into port */
	int bytes_written = 0;	/* Value for storing the number of bytes written to the port */ 
	int *fd;
	
	fd = (int *)arg;	/* Retrieve file descriptor */
	while(read_done != 1){
		sleep(2);	/* Wait for read to start first */
		bytes_written = write(*fd, write_buffer, 1);	/* use write() to send data to port */
	}

return NULL;
}
