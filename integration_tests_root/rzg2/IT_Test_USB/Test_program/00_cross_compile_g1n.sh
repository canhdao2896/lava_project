#!/bin/bash
#Function: Cross compile all Test Program
#Author: AnhT
#Version:
#Project: Qualified Solution
#Note: This tool run on Linux PC with SDK RZ/G1N

#=========================================================

CURPATH=`pwd`
sudo chmod 777 * -R
#Check SDK & export variables
if [ -f "/opt/poky/2.0.1/environment-setup-cortexa15hf-vfp-neon-poky-linux-gnueabi" ]
then
	source /opt/poky/2.0.1/environment-setup-cortexa15hf-vfp-neon-poky-linux-gnueabi
	export LDFLAGS=""
	export LIBSHARED=/opt/poky/2.0.1/sysroots/cortexa15hf-vfp-neon-poky-linux-gnueabi/usr/lib
	export BUILDDIR=/opt/poky/2.0.1/sysroots/cortexa15hf-vfp-neon-poky-linux-gnueabi/usr/include
	export KERNELSRC=/opt/poky/2.0.1/sysroots/cortexa15hf-vfp-neon-poky-linux-gnueabi/usr/src/kernel

else
	echo "Error: SDK missing"
	echo "Please install SDK into Host PC"
	exit 1
fi

echo "----------------------"
echo "Cross-compilation starts"
echo "----------------------"
echo ""

for tc_dir in `ls | egrep -v "K-BSP_list.csv|GPL-COPYING|K-BSP_common|Makefile|README.txt|\.sh|\.csh|include|tg|tgz"`
do
	cd $CURPATH/$tc_dir
	if [ -f "./Makefile" ]
	then
		make clean >& /dev/null
		make >& /dev/null
		if [ -f "./main" ]
		then
			echo "$CURPATH/$tc_dir:	Successful"
		else
			echo "$tc_dir: No main found"
		fi
	else
		echo "$CURPATH/$tc_dir:	No Makefile found"
	fi
done
