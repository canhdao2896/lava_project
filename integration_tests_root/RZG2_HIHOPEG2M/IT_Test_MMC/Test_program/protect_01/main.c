/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking protect system call
* Sequence: open; set_protect; clear_write_protect; close
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call protect with device: eMMC; mode: O_RDWR; then check set_protect with arg = 0x2EE000; call clear_protect with arg = 0x2EE000. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include <linux/mmc/ioctl.h>
#include "get_vendor_ID_and_device.h"

#include <signal.h>

#define MMC_BLOCK_MAJOR			179
#define MMC_SET_WRITE_PROT       28 
#define MMC_CLR_WRITE_PROT       29 
#define MMC_CMD_AC      (0 << 5)
#define MMC_RSP_PRESENT (1 << 0)
#define MMC_RSP_CRC     (1 << 2)               
#define MMC_RSP_BUSY    (1 << 3)               
#define MMC_RSP_OPCODE  (1 << 4)               
#define MMC_RSP_SPI_S1  (1 << 7)               
#define MMC_RSP_SPI_BUSY (1 << 10)             
#define MMC_RSP_SPI_R1B (MMC_RSP_SPI_S1|MMC_RSP_SPI_BUSY)
#define MMC_RSP_R1B     (MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE|MMC_RSP_BUSY)
void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct mmc_ioc_cmd	cmd;

	//Get mmc device from file ./board_config.txt
	char mmc_dev[12] = "/dev/";
	char *mmc_dev_get;
	mmc_dev_get = get_ethernet_device("MMC_DEV");
	strcat(mmc_dev, mmc_dev_get);
	int block_num = get_block_number("MMC_DEV");
	int block_size = get_block_size("MMC_DEV");
	
	/* Call API or system call follow describe in PCL */
	fd = open(mmc_dev, O_RDWR);

	/* Set block write protection */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_SET_WRITE_PROT;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1B | MMC_RSP_R1B | MMC_CMD_AC;
	result = ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

	/* Clear block write protection */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_CLR_WRITE_PROT;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1B | MMC_RSP_R1B | MMC_CMD_AC;
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);
	
	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

