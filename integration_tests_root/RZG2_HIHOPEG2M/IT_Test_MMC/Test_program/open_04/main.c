/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking open system call
* Sequence: open; close
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call open with device: eMMC; mode: 0xff. Expected result = NG
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include <linux/mmc/ioctl.h>
#include "get_vendor_ID_and_device.h"

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	//Get mmc device from file ./board_config.txt
	char mmc_dev[12] = "/dev/";
	char *mmc_dev_get;
	mmc_dev_get = get_ethernet_device("MMC_DEV");
	strcat(mmc_dev, mmc_dev_get);
	
	/* Call API or system call follow describe in PCL */
	result = open(mmc_dev, 0xff);

	/* Check return value of sequence */
	if (result >= 0) {
		printf("OK\n");
		close(result);
	} else {
		printf("NG\n");
	}
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

