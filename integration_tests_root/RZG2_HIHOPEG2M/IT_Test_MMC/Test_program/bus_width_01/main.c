/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking bus_width system call
* Sequence: open; bus_width; write; read; bus_width; close
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call bus_width with device: eMMC; mode: O_RDWR then change bus width to EXT_CSD_BUS_WIDTH_8; write; read; and restore default bus width. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include <linux/mmc/ioctl.h>
#include "get_vendor_ID_and_device.h"

#include <signal.h>

#define MMC_BLOCK_MAJOR			179
#define MMC_SWITCH                6 
#define MMC_SWITCH_MODE_WRITE_BYTE      0x03   
#define EXT_CSD_BUS_WIDTH               183    
#define EXT_CSD_BUS_WIDTH_1     0      
#define EXT_CSD_BUS_WIDTH_4     1      
#define EXT_CSD_BUS_WIDTH_8     2      
#define EXT_CSD_CMD_SET_NORMAL          (1<<0)
#define MMC_READ_SINGLE_BLOCK    17
#define MMC_WRITE_BLOCK          24   
#define MMC_CMD_AC      (0 << 5)
#define MMC_RSP_PRESENT (1 << 0)
#define MMC_RSP_CRC     (1 << 2)               
#define MMC_RSP_BUSY    (1 << 3)               
#define MMC_RSP_OPCODE  (1 << 4)               
#define MMC_CMD_ADTC    (1 << 5)
#define MMC_RSP_SPI_S1  (1 << 7)               
#define MMC_RSP_SPI_BUSY (1 << 10)             
#define MMC_RSP_SPI_R1B (MMC_RSP_SPI_S1|MMC_RSP_SPI_BUSY)
#define MMC_RSP_R1B     (MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE|MMC_RSP_BUSY)
#define MMC_RSP_R1      (MMC_RSP_PRESENT|MMC_RSP_CRC|MMC_RSP_OPCODE)
#define MMC_RSP_SPI_R1  (MMC_RSP_SPI_S1)
void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	int i;
	int diff_read_write = -1;
	struct mmc_ioc_cmd	cmd;

	//Get mmc device from file ./board_config.txt
	char mmc_dev[12] = "/dev/";
	char *mmc_dev_get;
	mmc_dev_get = get_ethernet_device("MMC_DEV");
	strcat(mmc_dev, mmc_dev_get);
	int block_num = get_block_number("MMC_DEV");
	int block_size = get_block_size("MMC_DEV");
	__u8 wbuf[block_size];
	__u8 rbuf[block_size];
	
	/* Call API or system call follow describe in PCL */
	fd = open(mmc_dev, O_RDWR);

	/* Change bus width setting */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_SWITCH;
	cmd.arg		= (MMC_SWITCH_MODE_WRITE_BYTE << 24) | (EXT_CSD_BUS_WIDTH << 16) | (EXT_CSD_BUS_WIDTH_8 << 8) | EXT_CSD_CMD_SET_NORMAL;
	cmd.flags	= MMC_RSP_SPI_R1B | MMC_RSP_R1B | MMC_CMD_AC;
	result = ioctl(fd, MMC_IOC_CMD, &cmd);
	if (result != 0)
		goto exit;

	/* Write 1st block */
	memset(&wbuf, 1, sizeof(wbuf));
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_WRITE_BLOCK;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	cmd.blksz	= block_size;	//Block size
	cmd.blocks	= 1;
	mmc_ioc_cmd_set_data(cmd, wbuf);
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

	/* Read 1st block */
	memset(&rbuf, 0, sizeof(rbuf));
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 0;
	cmd.opcode	= MMC_READ_SINGLE_BLOCK;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	cmd.blksz	= block_size;	//Block size
	cmd.blocks	= 1;
	mmc_ioc_cmd_set_data(cmd, rbuf);
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);
	
exit:
	/* Check return value of sequence */
	switch(result) {
	case 0:
		for (i = 0; i < block_size; i++) {
			if (rbuf[i] != wbuf[i]) {
				diff_read_write = 0;
				break;
			}
		}
		if (diff_read_write == 0)
			printf ("NG_diff_read_write\n");
		else
			printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};

	/* Restore default bus width setting */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_SWITCH;
	cmd.arg		= (MMC_SWITCH_MODE_WRITE_BYTE << 24) | (EXT_CSD_BUS_WIDTH << 16) | (EXT_CSD_BUS_WIDTH_4 << 8) | EXT_CSD_CMD_SET_NORMAL;
	cmd.flags	= MMC_RSP_SPI_R1B | MMC_RSP_R1B | MMC_CMD_AC;
	ioctl(fd, MMC_IOC_CMD, &cmd);

	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

