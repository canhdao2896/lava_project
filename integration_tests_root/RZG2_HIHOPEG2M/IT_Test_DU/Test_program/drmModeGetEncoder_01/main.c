/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking drmModeGetEncoder system call
* Sequence: drmOpen() -> drmModeGetConnector() -> drmModeGetEncoder()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874
* Details_description: External device: HDMI Display. Condition: Call drmModeGetEncoder to acquire HDMIA encoder info where Encoder ID is obtained from Connector with ID: 58 and Encoder object is expected to point to CRTC ID: 56. Expected result = OK
*/

#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModeConnector	*connector;
drmModeEncoder	*result;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get Connector info */
	connector = drmModeGetConnector(fd, 58);
	
	/* Get Encoder info of this connector */
	result = drmModeGetEncoder(fd, connector->encoder_id);
	
	/* Check return value of sequence */
	if(!result) {
		printf ("NG_GET_ENCODER_FAILED\n");
	}
	else if (result->crtc_id != 56){
		printf ("NG_WRONG_CRTC_ID\n");
		drmModeFreeEncoder(result);
	}
	else {
		printf ("OK\n");
		drmModeFreeEncoder(result);
	}

	/* Undo the drm setup */
	drmModeFreeConnector(connector);
	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

