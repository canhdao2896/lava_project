/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking drmModeGetConnector system call
* Sequence: drmOpen() -> drmModeGetConnector()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874
* Details_description: External device: HDMIA Display. Condition: Call drmModeGetConnector to acquire HDMIA connector info ; HDMIA connector ID is 58. Expected result = OK
*/
#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModeConnector	*result;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get Connector info of a specific Connector ID */
	result = drmModeGetConnector(fd, 58); /* Connector ID is 58 */
	
	/* Check return value of sequence */
	if(!result) {
		printf ("NG_GET_CONNECTOR_FAILED\n");
	}
	else if (result->connector_type != DRM_MODE_CONNECTOR_HDMIA){
		printf ("NG_WRONG_DISPLAY_TYPE\n");
		drmModeFreeConnector(result);
	}
	else if (result->connection != DRM_MODE_CONNECTED){
		printf ("NG_CONNECTOR_DISCONNECTED\n");
		drmModeFreeConnector(result);
	}
	else {
		printf ("OK\n");
		drmModeFreeConnector(result);
	}

	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

