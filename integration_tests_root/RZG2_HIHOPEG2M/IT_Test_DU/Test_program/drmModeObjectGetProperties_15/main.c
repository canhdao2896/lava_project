/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking drmModeObjectGetProperties system call
* Sequence: drmOpen() -> drmModeObjectGetProperties()
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2N
* Details_description: Condition: Call drmModeObjectGetProperties to acquire Connector properties where Connector ID is 11 (invalid). Expected result = NG
*/

#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModeObjectProperties	*result;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get properties of the target object */
	result = drmModeObjectGetProperties(fd, 11, DRM_MODE_OBJECT_CONNECTOR);

	/* Check return value of sequence */
	if(!result) {
		printf ("NG\n");
	}
	else {
		printf ("OK\n");
		drmModeFreeObjectProperties(result);
	}

	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

