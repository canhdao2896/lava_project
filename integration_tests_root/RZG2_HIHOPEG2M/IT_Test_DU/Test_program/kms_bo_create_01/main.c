/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking kms_bo_create system call
* Sequence: drmOpen() -> kms_create() -> kms_bo_create()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call kms_bo_create to create buffer object. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xf86drm.h>
#include <drm_fourcc.h>
#include <xf86drmMode.h>
#include <drm_mode.h>
#include <libkms.h>
#include <sys/errno.h>
#include <signal.h>

/* Declare global variable */
struct kms_driver *kms_driver;
struct kms_bo *bo;
int	result = -1;

unsigned attributes[7] = {
	KMS_WIDTH, 1920,
	KMS_HEIGHT, 1080,
	KMS_BO_TYPE, KMS_BO_TYPE_SCANOUT_X8R8G8B8,
	KMS_TERMINATE_PROP_LIST,
};

/* Declare signal handler */
void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main()
{
	/* Declare local variable */
	int fd;

	/* Initialize variable and assign value for variable */

	/* This block of code will catch segmentation fault error */
	struct sigaction sa;

	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);

	/* assign exception handler */
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;

	sigaction(SIGSEGV, &sa, NULL);

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);

	/* Create kms driver */
	kms_create(fd, &kms_driver);

	/* Create buffer object */
	result = kms_bo_create(kms_driver, attributes, &bo);

	/* Check return value of sequence */
	if(result) {
		printf ("NG\n");
	}
	else{
		printf ("OK\n");
		kms_bo_destroy(&bo);
	}

	/* Destroy kms driver */
	kms_destroy(&kms_driver);
	/* Close FB device */
	drmClose(fd);

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SEGFAULT\n");
	exit(0);
}
