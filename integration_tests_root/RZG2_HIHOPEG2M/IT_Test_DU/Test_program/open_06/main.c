/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking open system call
* Sequence: open()
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call open with supported device /dev/fb1 and supported mode O_WRONLY to access the framebuffer device. Expected result = NG
*/

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	result = open("/dev/fb1", O_WRONLY | O_NOCTTY);	/* fb0 is the framebuffer */
									/* O_RDWR, O_RDONLY or O_WRONLY - Read/Write access to serial port */
									/* O_NOCTTY - No terminal will control the process */
	/* Check return value of sequence */
	if(result >= 0) {
		printf ("OK\n");
	}
	else if(result == -1) {
		printf ("NG\n");
	}
	else{
		printf ("Unknown_result\n");
	}

	/* Close FB device */
	if(result >= 0)
		close(result);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

