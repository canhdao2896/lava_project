/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking pci_filter_init system call
* Sequence: pci_alloc();pci_filter_init();pci_init()
* Testing level: system call
* Test-case type: Normal
* Expected result: NG_SF
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: PCI card. Condition: Call API pci_filter_init of pciutils library with filter NULL. Use pci_init to test pci_filter_init. Expected result = NG_SF
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pci/pci.h>
#include "get_vendor_ID_and_device.h"

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(void)
{
	struct pci_access	*pci_acc;
	struct pci_dev		*device;
	char data[1024];
	int result;
	struct pci_filter filter;
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Get Vendor_ID of PCI Device from file ./board_config.txt
	int VENDOR_ID;
	VENDOR_ID = get_Vendor_ID_USB_PCI("PCI_ID");

	pci_acc = pci_alloc();	//Get pci_access
	pci_filter_init(pci_acc,NULL);
	pci_init(pci_acc);	//Initialize PCI library
	printf ("OK\n");
	pci_cleanup(pci_acc); //Close all
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}
