/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking pci_lookup_method system call
* Sequence: pci_alloc();pci_init();pci_scan_bus();pci_lookup_method()
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: PCI card. Condition: Call API pci_lookup_method of pciutils library un-support name xyz. Expected result = NG
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pci/pci.h>
#include "get_vendor_ID_and_device.h"

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(void)
{
	struct pci_access	*pci_acc;
	struct pci_dev		*device;
	char	data[1024];
	int	result;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Get Vendor_ID of PCI Device from file ./board_config.txt
	int VENDOR_ID;
	VENDOR_ID = get_Vendor_ID_USB_PCI("PCI_ID");

	pci_acc = pci_alloc();	//Get pci_access

	pci_init(pci_acc);	//Initialize PCI library

	pci_scan_bus(pci_acc);	//Scan all devices on PCI bus
	for (device=pci_acc->devices; device; device=device->next)      /* Iterate over all devices */
	{
		if (device->vendor_id == VENDOR_ID){
			break; /* detected PCIE card, access data done, break from "for" loop here*/
		}
	}	

	//device=pci_acc->devices;	//Chose the fisrt device
	//printf("vendor_id: %d\n",device->vendor_id);


	result = pci_lookup_method("xyz");
	//printf("result: %d\n",result);

	if(result >= 1){
		printf ("OK\n");
	}
	else{
		printf ("NG\n");
	}

	pci_cleanup(pci_acc); //Close all
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}
