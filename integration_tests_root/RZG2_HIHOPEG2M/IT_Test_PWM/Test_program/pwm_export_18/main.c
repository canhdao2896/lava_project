/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking pwm_export system call
* Sequence: pwm_export()
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call pwm_export to export PWM channel 7 into userspace (invalid PWM channel). Expected result = NG
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define BUF_SIZE	64

/* Export PWM to user space */
int pwm_export(unsigned int pwm)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/export", pwm);

	fd = open(buf, O_WRONLY);
	if (fd < 0)
	{
	        return fd;
	}
	if (write(fd, "1", 1) < 0)
	{
		return -1;
	}
	close(fd);
	
	return 0;
}

/* Free PWM */
int pwm_unexport(unsigned int pwm)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/unexport", pwm);

	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;

	if (write(fd, "0", 1) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int result = -1;
	int pwm = 7;
	
	/* Export the PWM into User Space */
	result = pwm_export(pwm);
	/* Check return value of sequence */
	if (result == 0) {
		printf ("OK\n");
	}
	else if (result < 0) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}
	
	/* Free the PWM */
	pwm_unexport(pwm);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

