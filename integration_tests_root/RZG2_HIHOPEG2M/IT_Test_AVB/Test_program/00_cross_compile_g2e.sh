#/bin/bash
#Function: Cross compile all Test Program
#Author: AnhT 
#Version:
#Project: Qualified Solution 
#Note: This tool run on Linux PC with SDK RZ/G1N

#=========================================================

CURPATH=`pwd`
sudo chmod 777 * -R
#Check SDK & export variables
if [ -f "/opt/poky/2.4.3/environment-setup-aarch64-poky-linux" ]
then
	source /opt/poky/2.4.3/environment-setup-aarch64-poky-linux
	export LDFLAGS=""
	export LIBSHARED=/opt/poky/2.4.3/sysroots/aarch64-poky-linux/usr/lib
	export BUILDDIR=/opt/poky/2.4.3/sysroots/aarch64-poky-linux/usr/include
        export KERNELSRC=/opt/poky/2.4.3/sysroots/aarch64-poky-linux/usr/src/kernel

else
	echo "Error: SDK missing"
	echo "Please install SDK into Host PC"
	exit 1
fi

echo "----------------------"
echo "Cross-compilation starts"
echo "----------------------"
echo ""

for tc_dir in `ls -l | egrep "^d" | awk '{print $NF}'`
do
	cd $CURPATH/$tc_dir
	if [ -f "./Makefile" ]
	then
		make clean >& /dev/null
		make >& /dev/null
		if [ -f "./main" ]
		then
			echo "$CURPATH/$tc_dir:	Successful"
		else
			echo "$tc_dir: No K_BSP_TP found"
		fi
	else
		echo "$CURPATH/$tc_dir:	No Makefile found"
	fi
done
