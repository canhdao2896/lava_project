/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking ioctl_SIOCGIFFLAGS system call
* Sequence: socket();ioctl_SIOCGIFFLAGS()
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_SIOCGIFFLAGS with device eth2 after call socket with input AF_INET; SOCK_STREAM; IPPROTO_TCP. Expected result = NG
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	struct ifreq	ifreq_dev;
	memset(&ifreq_dev, 0, sizeof(ifreq_dev));

	//Call set of functions under test
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	strncpy(ifreq_dev.ifr_name, "eth4", sizeof ifreq_dev.ifr_name);
	result = ioctl(fd, SIOCGIFFLAGS, &ifreq_dev);
	//printf("ifreq_dev.ifr_flags: %u\n",ifreq_dev.ifr_flags);

	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

