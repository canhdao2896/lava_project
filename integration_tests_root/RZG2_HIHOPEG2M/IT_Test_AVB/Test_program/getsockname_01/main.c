/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking getsockname system call
* Sequence: socket();getsockname()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call getsockname after call socket with input AF_INET; SOCK_STREAM; IPPROTO_TCP. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <fcntl.h>
#include <errno.h> /*to print string error*/

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	socklen_t addr_len;
	struct sockaddr local;
	memset(&local, 0, sizeof(local));
	//Call set of functions under test
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	addr_len = sizeof(local);
	result = getsockname(fd,(struct sockaddr *)&local,&addr_len);
//	printf("Error %s\n",strerror(errno));
	if( result < 0){
		printf("NG\n");
	}
	else{
		printf("OK\n");
	}
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

