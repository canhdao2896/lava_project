/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking ioctl_SIOCGIFMETRIC system call
* Sequence: socket();ioctl_SIOCGIFMETRIC()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_SIOCGIFMETRIC with device eth1 after call socket with input AF_INET; SOCK_STREAM; IPPROTO_TCP. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include "get_vendor_ID_and_device.h"

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	struct ifreq	ifreq_dev;
	memset(&ifreq_dev, 0, sizeof(ifreq_dev));

	//Get eth* device of AVB from file ./board_config.txt
	char  *eth_dev;
	eth_dev = get_ethernet_device("AVB_DEV");

	//Call set of functions under test
	fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	strncpy(ifreq_dev.ifr_name, eth_dev, sizeof ifreq_dev.ifr_name);
	result = ioctl(fd, SIOCGIFMETRIC, &ifreq_dev);
//	printf("ifreq_dev.ifr_metric: %d\n",ifreq_dev.ifr_metric);

	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

