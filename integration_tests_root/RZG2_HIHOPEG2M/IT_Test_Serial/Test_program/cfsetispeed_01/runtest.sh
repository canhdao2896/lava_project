#!/bin/sh
#get the current location - testcase name
#Maintainer: CanhDao
tc_dir=$1

echo "[test_start:test_case]"

result=`./main`

if [ $? = "0" ]
then
	if [ $result = "OK" ]
	then
		echo "$tc_dir+pass" >> ../log_file.txt #"+" is a separator will be used in send-to-lava.sh
	else
		echo "$tc_dir+fail+[test_log:TPREL_test_case_$result:Expected value mismatch with Real value. OK vs $result]" >> ../log_file.txt
	fi
else
	echo "$tc_dir+skip" >> ../log_file.txt
fi
