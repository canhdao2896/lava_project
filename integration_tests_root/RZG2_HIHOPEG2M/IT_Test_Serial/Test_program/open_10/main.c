/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking API open of Serial Debug driver
* Sequence: open()
* Testing level: API user lib
* Test-case type: Normal
* Expected result: OK
* Name: K_BSP_TP.c
* Author: RVC/HungDong (hung.dong.xd@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call open with supported device /dev/ttySC2 and supported mode O_WRONLY. Expected result = OK
*/

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int result = -1;

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	/* Call API or system call follow describe in PCL */
	result = open("/dev/ttySC2", O_WRONLY | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
									/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
									/* O_NOCTTY - No terminal will control the process */
									/* Open in blocking mode,read will wait */
	/* Check return value of sequence */
	if (result >= 0)
		printf ("OK\n");
	else if (result == -1)
		printf ("NG\n");
	else
		printf ("Unknown_result\n");
	
	/* Close tty device*/
	if (result != -1)
		close(result);

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

