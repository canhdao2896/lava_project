/*
* Project: CIP LAVA IT test
* Test ID: ioctl_VIDIOC_S_FMT_410
* Feature: Checking system call ioctl_VIDIOC_S_FMT of VIN driver
* Sequence: open() -> ioctl_VIDIOC_S_FMT()
* Testing level: API user lib
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_VIDIOC_S_FMT with device /dev/video7 to set the output data of VIN to color format - and capture widthxheight is -x- (where size of VIN input image is 720x480 and retrieved from struct v4l2_rect.bounds parameter). Expected result = NG
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>


#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int result = -1;

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct v4l2_format format;
	
	/* Initialize variable and assign value for variable */
	memset (&format, 0, sizeof(format));
	
	/* Call API or system call follow describe in PCL */
	fd = open("/dev/video7", O_RDWR);
	
	/* Setting scaling & format */
	result = ioctl(fd, VIDIOC_S_FMT, NULL);
	
	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	
	/* Close the device */
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

