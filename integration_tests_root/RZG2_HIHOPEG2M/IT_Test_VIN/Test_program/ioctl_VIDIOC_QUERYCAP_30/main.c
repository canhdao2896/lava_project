/*
* Project: CIP LAVA IT test
* Test ID: ioctl_VIDIOC_QUERYCAP_30
* Feature: Checking system call ioctl_VIDIOC_QUERYCAP of VIN driver
* Sequence: open() -> ioctl_VIDIOC_QUERYCAP()
* Testing level: API user lib
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_VIDIOC_QUERYCAP to obtain information about driver and hardware capabilities with device /dev/video7 where the capability argument is NULL. Expected result = NG
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>


#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int result = -1;

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct v4l2_capability	cap;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/video7", O_RDWR);

	result = ioctl(fd, VIDIOC_QUERYCAP, NULL);
	
	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	
	/* Close the device */
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

