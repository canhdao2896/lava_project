/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking gpio_unexport system call
* Sequence: gpio_export() -> gpio_unexport()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call gpio_unexport to free GPIO pin GP5_05 from userspace. Expected result = OK
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define BUF_SIZE	32

#include "gpio_pin_header_g2e.h"


/* Export GPIO to user space */
int gpio_export(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd < 0)
		return fd;

	sprintf(buf, "%d", gpio);
	
	if (write(fd, buf, strlen(buf)) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Free GPIO */
int gpio_unexport(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd < 0)
		return fd;
	
	sprintf(buf, "%d", gpio);
	
	if (write(fd, buf, strlen(buf)) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int	result = -1;
	int gpio = GP5_05;
	
	/* Export the GPIO into User Space */
	gpio_export(gpio);

	/* Free the GPIO */
	result = gpio_unexport(gpio);
	
	/* Check return value of sequence */
	if (result == 0) {
		printf ("OK\n");
	}
	else if (result < 0) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

