/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking ALSA API ssi_capture_rate of SSI
* Sequence: Call ssi_capture_rate
* Testing level: ALSA API
* Test-case type: NORMAL
* Expected result: OK
* Name: main.c
* Author: RVC/TinLe (tin.le.wh@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874
* Details_description: Condition: Call ssi_capture_rate
*/
#include <alsa/asoundlib.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

unsigned char *buffer;

int main() {
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int result;
	int size;
	unsigned int i;
	snd_pcm_t *handle;
	snd_pcm_sframes_t frames = 1;
	
	snd_pcm_open(&handle, "default", SND_PCM_STREAM_CAPTURE, 0);
	if((result = snd_pcm_set_params(handle,
					SND_PCM_FORMAT_S16_LE,
					SND_PCM_ACCESS_RW_INTERLEAVED,
					2,
					48000,
					1,
					600000)) < 0) {
		printf("SET_PARAMS_ERROR\n");
		return 0;
	} 
	
	size = frames * 2 * snd_pcm_format_width(SND_PCM_FORMAT_S16_LE) * 2;
	buffer =(char *)malloc(size);
	
	result = snd_pcm_readi(handle, buffer, frames);
	
	if(result > 0) 
		printf("OK\n");	
	else
		printf("ERROR\n");
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

