/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking ALSA API ssi_capture_mmap_interleaved of SSI
* Sequence: Call ssi_capture_mmap_interleaved
* Testing level: ALSA API
* Test-case type: NORMAL
* Expected result: OK
* Name: main.c
* Author: RVC/TinLe (tin.le.wh@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874
* Details_description: Condition: Call ssi_capture_mmap_interleaved
*/
#include <alsa/asoundlib.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main() {
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int result;
	int size;
	int channels = 1;
	snd_pcm_t *handle;
	snd_pcm_hw_params_t *hw_params;
	unsigned int val = 44100;
	int dir = 0;
	snd_pcm_uframes_t frames = 1;
	char *buffer;

	snd_pcm_open(&handle, "default", SND_PCM_STREAM_CAPTURE, 0);
	snd_pcm_hw_params_alloca(&hw_params);
	snd_pcm_hw_params_any(handle, hw_params);
	snd_pcm_hw_params_set_access(handle, hw_params, SND_PCM_ACCESS_MMAP_INTERLEAVED);

	if((result = snd_pcm_hw_params_set_format(handle, hw_params, SND_PCM_FORMAT_S8)) < 0) {
		printf("SET_FORMAT_ERROR\n");
		return 0;
	}

	snd_pcm_hw_params_set_channels(handle, hw_params, channels);
	snd_pcm_hw_params_set_rate_near(handle, hw_params, &val, &dir);
	snd_pcm_hw_params_set_period_size_near(handle, hw_params, &frames, &dir);

	if((result = snd_pcm_hw_params(handle, hw_params)) < 0) {
		printf("HW_PARAMS_ERROR\n");
		return 0;
	}

	snd_pcm_hw_params_get_period_size(hw_params, &frames, &dir);
	snd_pcm_hw_params_get_period_time(hw_params, &val, &dir);
	
	size = frames * 2 * snd_pcm_format_width(SND_PCM_FORMAT_S8) * channels;
	buffer =(char *) malloc(size);

	result = snd_pcm_mmap_readi(handle, buffer, frames);

	snd_pcm_drain(handle);
	snd_pcm_close(handle);
	free(buffer);

	if(result > 0) 
		printf("OK\n");	
	else
		printf("ERROR\n");
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

