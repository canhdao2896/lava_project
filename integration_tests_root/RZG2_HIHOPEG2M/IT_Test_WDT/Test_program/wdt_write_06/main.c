/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking wdt_write system call
* Sequence: 
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2N
* Details_description: Condition: Call Write device /dev/watchdog0 with mode O_RDWR. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include <linux/watchdog.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int flags;
	int fd = -1;
	fd = open("/dev/watchdog0", O_RDWR);

	const char write_buffer = 'V';
	result = write(fd, &write_buffer, 1);

	flags = WDIOS_DISABLECARD;
	ioctl(fd, WDIOC_SETOPTIONS, &flags);

	/* Check return value of sequence */
	if (result >= 0) {
		printf("OK\n");
		close(fd);
	} else {
		printf("NG\n");
	}
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

