/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking ths_fclose system call
* Sequence: 
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2N
* Details_description: Condition: Call Close device /sys/class/thermal/thermal_zone0/temp. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	FILE	*fd;
	int	result = -1;
	int 	size = 10;
	char	get_temp_value[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	fd = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
//	printf("%d\n", fd);	//For debug only

	result = fclose(fd);
	/* Check return value of sequence */
	if (result == 0) {
		printf ("OK\n");
	}
	else if (fd < 0) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

