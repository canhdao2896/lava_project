#!/bin/sh
#Function: Run all Test Program
#Author: AnhT
#Version:
#Project: Qualified Solution
#Note: This tool run on Terminal of Board

#=========================================================

CURPATH=`pwd`

#LocNg: create new log file, backup the old one-----------------
if [ -f ./log_file.txt ]
then
	mv log_file.txt log_file_backup.txt
else
	touch log_file.txt
fi
#end modifying LocNg -------------------------------------------

for tc_dir in `ls | egrep -v "K-BSP_list.csv|GPL-COPYING|K-BSP_common|Makefile|README.txt|\.sh|\.csh|lib|include|tg|tgz|log|\.sh|\.csh|\.yaml"` #LocNguyen: added |\.sh|\.csh|\.yaml
do
	echo $tc_dir
	cd $CURPATH/$tc_dir
	if [ -f "./test_config.sh" ]
	then
		./test_config.sh
	fi

	./runtest.sh $tc_dir #LocNguyen change name of .sh file, added $tc_dir

	if [ -f "./test_config_restore.sh" ]
	then
		./test_config_restore.sh
	fi

done
