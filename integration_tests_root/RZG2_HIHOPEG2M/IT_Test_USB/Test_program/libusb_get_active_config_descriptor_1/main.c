/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking libusb_get_active_config_descri system call
* Sequence: libusb_init();libusb_get_device_list();libusb_get_device_descriptor();libusb_get_active_config_descriptor()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_get_active_config_descriptor of usblib library. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	int result = -1;
	int i;
	struct libusb_context *ctx;
	struct libusb_device **dev_list;
	struct libusb_device_descriptor desc;
	struct libusb_device_handle *handle = NULL;
	struct libusb_config_descriptor *config;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Main checking item
	libusb_init(&ctx);
	libusb_get_device_list(ctx, &dev_list);
	//libusb_free_device_list(dev_list, 0);
	for (i = 0; dev_list[i]; ++i) {
		libusb_device *dev = dev_list[i];
		libusb_get_device_descriptor(dev, &desc);
		//result = libusb_open(dev, &handle);
		result = libusb_get_active_config_descriptor(dev, &config);
		if( result != 0 ) {
			goto Exit;
		}
		else
		{
			libusb_free_config_descriptor(config);
		}
	}

	libusb_free_device_list(dev_list, 1);
	libusb_exit(ctx);
Exit:
	if( result == 0 ) {
		printf ("OK\n");
	} else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


