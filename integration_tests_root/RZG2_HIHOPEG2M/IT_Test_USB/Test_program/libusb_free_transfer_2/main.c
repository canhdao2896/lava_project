/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking libusb_free_transfer system call
* Sequence: libusb_init();libusb_set_debug();libusb_get_device_list();libusb_get_device_descriptor();libusb_get_config_descriptor();libusb_free_config_descriptor();libusb_open_device_with_vid_pid();libusb_free_device_list();libusb_kernel_driver_active();libusb_detach_kernel_driver(0;libusb_claim_interface();libusb_alloc_transfer();libusb_fill_bulk_transfer();libusb_free_transfer();libusb_release_interface();libusb_reset_device();libusb_close();libusb_exit();
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_free_transfer of usblib library with transfer NULL. Expected result = OK
*/
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include "get_vendor_ID_and_device.h"

#define USB_ENDPOINT_IN (LIBUSB_ENDPOINT_IN | 1) /* endpoint address */
#define USB_ENDPOINT_OUT (LIBUSB_ENDPOINT_OUT | 2) /* endpoint address */
#define LEN_IN_BUFFER 1024*8
static uint8_t in_buffer[LEN_IN_BUFFER];

uint32_t benchPackets = 1;
uint32_t benchBytes = 0;
struct timespec t1, t2;
uint32_t diff = 0;

struct libusb_transfer *transfer = NULL;

void segfault_sigaction(int signal, siginfo_t *si, void *arg);
void cb_in(struct libusb_transfer *transfer)
{
	//measure the time
	clock_gettime(CLOCK_REALTIME, &t2);
	//submit the next transfer
	libusb_submit_transfer(transfer);

	benchBytes += transfer->actual_length;
	//this averages the bandwidth over many transfers
	if(++benchPackets%100==0){
		//Warning: uint32_t has a max value of 4294967296 so this will overflow over 4secs
		diff = (t2.tv_sec-t1.tv_sec)*1000000000L+(t2.tv_nsec-t1.tv_nsec);
		t1.tv_sec = t2.tv_sec;
		t1.tv_nsec = t2.tv_nsec;
		printf("\rreceived %5d transfers and %8d bytes in %8d us, %8.1f B/s", benchPackets, benchBytes, diff/1000, benchBytes*1000000.0				/(diff/1000));
		fflush(stdout);
		benchPackets=0;
		benchBytes=0;
	}
}

int main(int argc, char *argv[])
{
	struct libusb_device_handle *handle;
	int i, result;
	struct libusb_context *ctx;
	struct libusb_device **dev_list;
	struct libusb_device_descriptor desc;
	uint16_t idVendor;
	uint16_t idProduct;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Get Vendor_ID of USB Device from file ./board_config.txt
	int device_vendor_id;
	device_vendor_id = get_Vendor_ID_USB_PCI("USB_ID");
	//Main checking item
	libusb_init(&ctx);
	if (result < 0) {
		printf("Error: libusb_init");
		return 0;
	}
	libusb_set_debug(ctx, 3);

	ssize_t cnt = libusb_get_device_list(ctx, &dev_list);
	if(cnt < 0) {
		printf("Error: libusb_get_device_list");
		return 0;
	}
	for (i = 0; dev_list[i]; ++i) {
		libusb_device *dev = dev_list[i];
		libusb_get_device_descriptor(dev, &desc);
		if(desc.idVendor == device_vendor_id) {
			idVendor = desc.idVendor;
			idProduct = desc.idProduct;
		}
	struct libusb_config_descriptor *config;
	libusb_get_config_descriptor(dev, 0, &config);
	libusb_free_config_descriptor(config);
	}

	handle = libusb_open_device_with_vid_pid(ctx, idVendor, idProduct);
	if(handle == NULL)
	{
		printf("Error: libusb_open_device_with_vid_pid\n");
		return 0;
	}

	libusb_free_device_list(dev_list, 1);

	result = libusb_kernel_driver_active(handle, 0);//If kernel_driver active, we can't call libusb_claim_interface
	if (result == 1)
	{
		if(libusb_detach_kernel_driver(handle, 0) == 0) //detach kernel_driver
		{
			printf("Error: libusb_detach_kernel_driver\n");
		}	
	}

	result = libusb_claim_interface(handle, 0);
	if(result < 0) {
		printf("Error: libusb_claim_interface\n");
		return 0;
	}

	transfer = libusb_alloc_transfer(0);

	libusb_fill_bulk_transfer( transfer, handle, USB_ENDPOINT_IN,
				in_buffer, LEN_IN_BUFFER, 	// Note: in_buffer is where input data written.
					cb_in, NULL, 0); 		// no user data
	
	clock_gettime(CLOCK_REALTIME, &t1);			//take the initial time measurement
	
	// Test function libusb_free_transfer() with transfer NULL
	libusb_free_transfer(NULL);				// Transfers are deallocated when transfer was not used

	libusb_release_interface(handle, 0);			//release the claimed interface
	libusb_reset_device(handle);
	libusb_close(handle);
	libusb_exit(ctx);

	printf ("OK\n");

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


