/*
* Project: RZG_IT_CIP_BSP
* Test ID: test_case
* Feature: Checking ioctl_SIOCSIFNAME system call
* Sequence: socket();ioctl_SIOCSIFNAME()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_SIOCSIFNAME with ifr_ifindex = 2 after call socket with input PF_CAN; SOCK_RAW; CAN_RAW. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>


#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	char   old_name[8];
	struct ifreq	ifreq_dev;
	memset(&ifreq_dev, 0, sizeof(ifreq_dev));

	//Call set of functions under test
	fd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	ifreq_dev.ifr_ifindex = 2;
	ioctl(fd, SIOCGIFNAME, &ifreq_dev);
	strncpy(old_name, ifreq_dev.ifr_name, sizeof(old_name));
	strncpy(ifreq_dev.ifr_newname, "new_can", sizeof ifreq_dev.ifr_newname);
//	printf("device name is %s\n", ifreq_dev.ifr_name); //Expected: can1
	result = ioctl(fd, SIOCSIFNAME, &ifreq_dev);
	ioctl(fd, SIOCGIFNAME, &ifreq_dev);
	/*re-change to old name*/
	ifreq_dev.ifr_ifindex = 2;
	ioctl(fd, SIOCGIFNAME, &ifreq_dev);
	strncpy(ifreq_dev.ifr_newname, old_name, sizeof ifreq_dev.ifr_newname);
        ioctl(fd, SIOCSIFNAME, &ifreq_dev);
//	ioctl(fd, SIOCGIFNAME, &ifreq_dev);

//	printf("device name is %s\n", ifreq_dev.ifr_name); //Expected: can0

	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

